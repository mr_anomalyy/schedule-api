<?php

declare(strict_types=1);
require_once("db.php");
require_once("authHelper.php");
require_once("statusEnum.php");
require_once("responseHelper.php");
require_once("scheduleHelper.php");
require_once("routes/root.php");

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;

return function (App $app) {
  AuthRoute::set($app);
  ScheduleByDateRoute::set($app);
  CabinetsRoute::set($app);
  StudygroupsRoute::set($app);
  TeachersRoute::set($app);
};
