<?php

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class AuthRoute
{
  static function set(App $app)
  {
    $app->map(["GET", "PUT", "PATCH", "DELETE", "OPTIONS"], "/api/auth", function (Request $request, Response $response) {
      $response->getBody()->write(json_encode([
        "status" => StatusEnum::ERR,
        "errorText" => "Bad request",
        "errorCode" => "UNSUPPORTED_REQUEST_METHOD"
      ]));
      return $response;
    });
    $app->post("/api/auth", function (Request $request, Response $response, $args) {
      $pbody = $request->getParsedBody();
      $login = $pbody["login"] ?? null;
      $password = $pbody["password"] ?? null;
      if (empty($password) || empty($login)) {
        return ResponseHelper::BadInputResponse($response);
      } else {
        $db = new Db();
        $search = $db->getData("SELECT ID FROM accounts WHERE login='{$login}' AND password='{$password}'");

        if (count($search) < 1) {
          $response->getBody()->write(json_encode([
            "status" => StatusEnum::ERR,
            "errorText" => "Wrong login or password",
            "errorCode" => "WRONG_CREDENTIALS"
          ]));
        } else {
          $token = AuthHelper::GetNewToken($login);
          $db->doSql("INSERT INTO tokens (token) VALUES ('{$token}')");
          $response->getBody()->write(json_encode([
            "status" => StatusEnum::OK,
            "token" => $token
          ]));
        }
      }
      return $response;
    });
  }
}