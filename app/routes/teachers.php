<?php

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class TeachersRoute
{
  static function set(App $app)
  {
    $app->get('/api/teachers', function (Request $request, Response $response, $args) {

      if (!AuthHelper::Authorize($request)) {
        return ResponseHelper::BadTokenResponse($response);
      }

      $db = new Db();
      $data = $db->getData("SELECT * FROM teachers");

      if (count($data) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No teachers found",
          "errorCode" => "NO_TEACHERS_FOUND"
        ]));
      } else {
        $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE));
      }
      return $response;
    });

    $app->post('/api/teachers/{teacher}', function (Request $request, Response $response, $args) {
      $teacher = $args["teacher"];
      $db = new Db();
      $search = $db->getData("SELECT * FROM teachers WHERE name='{$teacher}'");
      if (count($search) > 0) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "Teacher already exist: ID " . $search[0]["ID"],
          "errorCode" => "TEACHER_ALREADY_EXIST"
        ]));
      } else {
        $db->doSql("INSERT INTO teachers (`name`) VALUES ('{$teacher}')");
        $id = $db->getData("SELECT LAST_INSERT_ID() as id FROM teachers LIMIT 1");
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::OK,
          "id" => $id[0]["id"]
        ]));
      }
      return $response;      
    });

    $app->put('/api/teachers/{teacher}', function (Request $request, Response $response, $args) {
      parse_str(file_get_contents("php://input"), $_REQVARS);
      $data = $_REQVARS["data"] ?? null;
      $teacher = $args["teacher"];
      if (empty($data)) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "Empty payload",
          "errorCode" => "EMPTY_PAYLOAD"
        ]));
      } else {
        if (!ScheduleHelper::VerifyTeacher($teacher)) {
          $response->getBody()->write(json_encode([
            "status" => StatusEnum::ERR,
            "errorText" => "No teacher found",
            "errorCode" => "NO_TEACHER_FOUND"
          ]));
        } else {
          $db = new Db();
          $search = $db->getData("SELECT * FROM teachers WHERE name='{$data}'");
          if (count($search) > 0) {
            $response->getBody()->write(json_encode([
              "status" => StatusEnum::ERR,
              "errorText" => "Target teacher name already exist: ID " . $search[0]["ID"],
              "errorCode" => "TEACHER_ALREADY_EXIST"
            ]));
          } else {
            $db->doSql("UPDATE teachers SET name='{$data}' WHERE name='{$teacher}'");
            $search = $db->getData("SELECT * FROM teachers WHERE name='{$data}'");
            $response->getBody()->write(json_encode([
              "status" => StatusEnum::OK,
              "id" => $search[0]["ID"],
              "name" => $search[0]["name"]
            ]));
          }
        }
      }
      return $response;
    });

    $app->delete('/api/teachers/{studygroup}', function (Request $request, Response $response, $args) {
      $db = new Db();
      $studygroup = $args["studygroup"];
      $search = $db->getData("SELECT * FROM teachers WHERE name='{$studygroup}'");
      if (count($search) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No group found",
          "errorCode" => "NO_GROUP_FOUND"
        ]));
      } else if (count($search) > 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "More than one studygroup found. Cannot continue operation.",
          "errorCode" => "MORE_THAN_ONE_STUDYGROUP_FOUND"
        ]));
      } else {
        $id = $search[0]["ID"];
        $db->doSql("DELETE FROM teachers WHERE ID='{$id}'");
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::OK,
          "id" => $id,
        ]));
      }
      return $response;
    });

  }
}
