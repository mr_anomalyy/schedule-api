<?php

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class cabinetsRoute
{
  static function set(App $app)
  {
    $app->get('/api/cabinets', function (Request $request, Response $response, $args) {
      if (!AuthHelper::Authorize($request)) {
        return ResponseHelper::BadTokenResponse($response);
      }

      $db = new Db();
      $data = $db->getData("SELECT * FROM cabinets");

      if (count($data) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No cabinets found",
          "errorCode" => "NO_CABINETS_FOUND"
        ]));
      } else {
        $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE));
      }
      return $response;
    });

    $app->post('/api/cabinets/{block}/{number}', function (Request $request, Response $response, $args) {
      $block = $args["block"];
      $number = $args["number"];
      $db = new Db();
      $search = $db->getData("SELECT * FROM cabinets WHERE block='{$block}' AND number='{$number}'");
      if (count($search) > 0) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "Cabinet already exist: ID " . $search[0]["ID"],
          "errorCode" => "CABINET_ALREADY_EXIST"
        ]));
      } else {
        $db->doSql("INSERT INTO cabinets (`block`, `number`) VALUES ('{$block}', '{$number}')");
        $id = $db->getData("SELECT LAST_INSERT_ID() as id FROM cabinets LIMIT 1");
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::OK,
          "id" => $id[0]["id"]
        ]));
      }
      return $response;      
    });

    $app->put('/api/cabinets/{block}/{number}', function (Request $request, Response $response, $args) {
      parse_str(file_get_contents("php://input"), $_REQVARS);
      $data = $_REQVARS["data"] ?? null;
      $data = json_decode($data);
      $block = $args["block"];
      $number = $args["number"];
      if (empty($data)) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "Empty payload",
          "errorCode" => "EMPTY_PAYLOAD"
        ]));
      } else {
        if (!ScheduleHelper::VerifyCabinet($block, $number)) {
          $response->getBody()->write(json_encode([
            "status" => StatusEnum::ERR,
            "errorText" => "No cabinet found",
            "errorCode" => "NO_CABINETS_FOUND"
          ]));
        } else {
          $db = new Db();
          $search = $db->getData("SELECT * FROM cabinets WHERE block='{$data->block}' AND number='{$data->number}'");
          if (count($search) > 0) {
            $response->getBody()->write(json_encode([
              "status" => StatusEnum::ERR,
              "errorText" => "Target cabinet name already exist: ID " . $search[0]["ID"],
              "errorCode" => "CABINET_ALREADY_EXIST"
            ]));
          } else {
            $db->doSql("UPDATE cabinets SET block='{$data->block}', number='{$data->number}' WHERE block='{$block}' AND number='{$number}'");
            $search = $db->getData("SELECT * FROM cabinets WHERE block='{$data->block}' AND number='{$data->number}'");
            $response->getBody()->write(json_encode([
              "status" => StatusEnum::OK,
              "id" => $search[0]["ID"],
              "block" => $search[0]["block"],
              "number" => $search[0]["number"],
            ], JSON_UNESCAPED_UNICODE));
          }
        }
      }
      return $response;
    });

    $app->delete('/api/cabinets/{block}/{number}', function (Request $request, Response $response, $args) {
      $db = new Db();
      $block = $args["block"];
      $number = $args["number"];
      $search = $db->getData("SELECT * FROM cabinets WHERE block='{$block}' AND number='{$number}'");
      if (count($search) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No cabinet found",
          "errorCode" => "NO_CABINETS_FOUND"
        ]));
      } else if (count($search) > 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "More than one cabinets found. Cannot continue operation.",
          "errorCode" => "MORE_THAN_ONE_CABINET_FOUND"
        ]));
      } else {
        $id = $search[0]["ID"];
        $db->doSql("DELETE FROM cabinets WHERE ID='{$id}'");
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::OK,
          "id" => $id,
        ]));
      }
      return $response;
    });

  }
}
