<?php

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class StudygroupsRoute
{
  static function set(App $app)
  {
    $app->get('/api/studygroups', function (Request $request, Response $response, $args) {
      $db = new Db();
      $data = $db->getData("SELECT * FROM studygroups");

      if (count($data) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No groups found",
          "errorCode" => "NO_GROUPS_FOUND"
        ]));
      } else {
        $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE));
      }
      return $response;
    });

    $app->post('/api/studygroups/{studygroup}', function (Request $request, Response $response, $args) {
      $studygroup = $args["studygroup"];
      $db = new Db();
      $search = $db->getData("SELECT * FROM studygroups WHERE name='{$studygroup}'");
      if (count($search) > 0) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "Studygroup already exist: ID " . $search[0]["ID"],
          "errorCode" => "STUDYGROUP_ALREADY_EXIST"
        ]));
      } else {
        $db->doSql("INSERT INTO studygroups (`name`) VALUES ('{$studygroup}')");
        $id = $db->getData("SELECT LAST_INSERT_ID() as id FROM studygroups LIMIT 1");
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::OK,
          "id" => $id[0]["id"]
        ]));
      }
      return $response;      
    });

    $app->put('/api/studygroups/{studygroup}', function (Request $request, Response $response, $args) {
      parse_str(file_get_contents("php://input"), $_REQVARS);
      $data = $_REQVARS["data"] ?? null;
      $studygroup = $args["studygroup"];
      if (empty($data)) {
        return ResponseHelper::BadInputResponse($response);
      } else {
        if (!ScheduleHelper::VerifyStudygroup($studygroup)) {
          $response->getBody()->write(json_encode([
            "status" => StatusEnum::ERR,
            "errorText" => "No group found",
            "errorCode" => "NO_GROUP_FOUND"
          ]));
        } else {
          $db = new Db();
          $search = $db->getData("SELECT * FROM studygroups WHERE name='{$data}'");
          if (count($search) > 0) {
            $response->getBody()->write(json_encode([
              "status" => StatusEnum::ERR,
              "errorText" => "Target studygroup name already exist: ID " . $search[0]["ID"],
              "errorCode" => "STUDYGROUP_ALREADY_EXIST"
            ]));
          } else {
            $db->doSql("UPDATE studygroups SET name='{$data}' WHERE name='{$studygroup}'");
            $search = $db->getData("SELECT * FROM studygroups WHERE name='{$data}'");
            $response->getBody()->write(json_encode([
              "status" => StatusEnum::OK,
              "id" => $search[0]["ID"],
              "name" => $search[0]["name"]
            ]));
          }
        }
      }
      return $response;
    });

    $app->delete('/api/studygroups/{studygroup}', function (Request $request, Response $response, $args) {
      $db = new Db();
      $studygroup = $args["studygroup"];
      $search = $db->getData("SELECT * FROM studygroups WHERE name='{$studygroup}'");
      if (count($search) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No group found",
          "errorCode" => "NO_GROUP_FOUND"
        ]));
      } else if (count($search) > 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "More than one studygroup found. Cannot continue operation.",
          "errorCode" => "MORE_THAN_ONE_STUDYGROUP_FOUND"
        ]));
      } else {
        $id = $search[0]["ID"];
        $db->doSql("DELETE FROM studygroups WHERE ID='{$id}'");
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::OK,
          "id" => $id,
        ]));
      }
      return $response;
    });

  }
}
