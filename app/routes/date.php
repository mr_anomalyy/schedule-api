<?php

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ScheduleByDateRoute
{
  static function set(App $app)
  {
    $app->any("/api/date", function (Request $request, Response $response) {
      $response->getBody()->write(json_encode([
        "status" => StatusEnum::ERR,
        "errorText" => "Empty input",
        "errorCode" => "EMPTY_INPUT"
      ]));
      return $response;
    });

    $app->get("/api/date/{date}", function (Request $request, Response $response, $args) {
      return self::GetScheduleByDateAndGroup($request, $response, $args);
    });
    $app->get("/api/date/{date}/{studygroup}", function (Request $request, Response $response, $args) {
      return self::GetScheduleByDateAndGroup($request, $response, $args);
    });
    $app->post("/api/date/{date}/{studygroup}", function (Request $request, Response $response, $args) {
      if (!AuthHelper::Authorize($request)) {
        return ResponseHelper::BadTokenResponse($response);
      }

      $data = json_decode($request->getParsedBody()["data"]);
      if ($data === null || !ScheduleHelper::VerifyScheduleList($data)) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "Bad payload",
          "errorCode" => "BAD_PAYLOAD"
        ]));
        return $response;
      }
      
      $datetime = strtotime($args["date"]);
      $studygroup = $args["studygroup"];
      if (!$datetime || !ScheduleHelper::VerifyStudygroup($studygroup)) {
        return ResponseHelper::BadInputResponse($response);
      }
      $date = date("Y-m-d", $datetime);

      $db = new Db();
      $search = $db->getData("SELECT ID, UID FROM schedule WHERE date='{$date}' AND studgroup='{$studygroup}'");
      
      if (count($search) > 0) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "A schedule exist for this date and studygroup. To update a line use a PUT method instead",
          "errorCode" => "SCHEDULE_ALREADY_EXIST",
          "id" => $search[0]["ID"],
          "uid" => $search[0]["UID"]
        ]));
        return $response;
      }
      
      $json = json_encode(ScheduleHelper::BuildScheduleObject($date, $studygroup, $data), JSON_UNESCAPED_UNICODE);
      $uid = ScheduleHelper::GetUID($json);
      $sql = "INSERT INTO schedule (uid, date, studgroup, json) VALUES ('{$uid}', '{$date}', '{$studygroup}', '{$json}')";
    
      $db->doSql($sql);

      $response->getBody()->write(
        json_encode(
          [
            "status" => StatusEnum::OK,
            "uid" => $uid
          ]
        )
      );
      return $response;
    });

    $app->put("/api/date/{date}/{studygroup}", function (Request $request, Response $response, $args) {
      $datetime = strtotime($args["date"]);
      $studygroup = $args["studygroup"];
      if (!$datetime || !ScheduleHelper::VerifyStudygroup($studygroup)) {
        return ResponseHelper::BadInputResponse($response);
      }
      $datetime = date("Y-m-d", $datetime);
      parse_str(file_get_contents("php://input"), $_REQVARS);
      $data = $_REQVARS["data"] ?? null;
      $data = json_decode($data);

      if (!$data || !ScheduleHelper::VerifyScheduleList($data)) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "Bad payload",
          "errorCode" => "BAD_PAYLOAD"
        ]));
        return $response;
      }
      $db = new Db();
      $search = $db->getData("SELECT ID, UID FROM schedule WHERE date='{$datetime}' AND studgroup='{$studygroup}'");
      
      if (count($search) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No schedule found",
          "errorCode" => "NO_SCHEDULE_FOUND"
        ]));
        return $response;
      } else if (count($search) > 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "More than one schedule found for this date. Operation canceled",
          "errorCode" => "MORE_THAN_ONE_SCHEDULE_EXIST"
        ]));
        return $response;
      } else {
        $id = $search[0]["ID"];
        $json = json_encode(ScheduleHelper::BuildScheduleObject($datetime, $studygroup, $data), JSON_UNESCAPED_UNICODE);
        $uid = ScheduleHelper::GetUID($json);
        $db->doSql("UPDATE schedule SET UID='{$uid}', json='{$json}' WHERE ID={$id}");
        $response->getBody()->write(
          json_encode([
            "status" => StatusEnum::OK,
            "id" => $search[0]["ID"],
            "uid" => $search[0]["UID"]
          ])
        );
        return $response;
      }
      return $response;
    });

    $app->delete("/api/date/{date}/{studygroup}", function (Request $request, Response $response, $args) {
      $datetime = strtotime($args["date"]);
      $studygroup = $args["studygroup"];
      if (!$datetime || !ScheduleHelper::VerifyStudygroup($studygroup)) {
        return ResponseHelper::BadInputResponse($response);
      }
      $datetime = date("Y-m-d", $datetime);
      $db = new Db();
      $search = $db->getData("SELECT ID, UID FROM schedule WHERE date='{$datetime}' AND studgroup='{$studygroup}'");
      if (count($search) < 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "No schedule found",
          "errorCode" => "NO_SCHEDULE_FOUND"
        ]));
      } else if (count($search) > 1) {
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::ERR,
          "errorText" => "More than one schedule line found. Canceling operation.",
          "errorCode" => "MORE_THAN_ONE_SCHEDULE_EXIST"
        ]));
      } else {
        $id = $search[0]["ID"];
        $db->doSql("DELETE FROM schedule WHERE ID='{$id}'");
        $response->getBody()->write(json_encode([
          "status" => StatusEnum::OK,
          "id" => $id,
          "uid" => $search[0]["UID"]
        ]));
      }
      return $response;
    });
  }

  static function GetScheduleByDateAndGroup(Request $request, Response $response, $args) {
    $datetime = strtotime($args["date"]);

    if (!$datetime) {
      return ResponseHelper::BadInputResponse($response);
    }

    $date = date("Y-m-d", strtotime($args["date"]));

    $db = new Db();
    $sql = "SELECT * FROM schedule WHERE date='{$date}'";

    if (!empty($args["studygroup"])) {
      $studygroup = $args["studygroup"];
      $sql .= " AND studgroup='{$studygroup}'";
    }

    $data = $db->getData($sql);
    if (!empty($data)) {
      $data["type"] = "changes";
      $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE));
    } else {
      $adate = ScheduleHelper::GetCurrentAcademicDate();
      $data = $db->getData("SELECT * FROM main WHERE date='{$adate}'");
      if (!empty($data)){
        $data["type"] = "main";
        $response->getBody()->write(json_encode($data, JSON_UNESCAPED_UNICODE));
      }
      $response->getBody()->write(json_encode([
        "error" => 404,
        "errorText" => "No schedule for this date",
        "errorCode" => "NO_SCHEDULE_FOUND"
      ]));
    }
    return $response;
  }
}
