<?php

require_once("db.php");

class ScheduleHelper {
  static public function GetCurrentWeek() {
    $db = new Db();
    $data = $db->getData("SELECT week FROM week WHERE ID=0");
    return $data[0]["week"];
  }
  
  static public function GetCurrentAcademicYear() {
    return date("m") < 9 ?
      date("Y", strtotime("-1 year")) . "-" . date("Y") :
      date("Y") . "-" . date("Y", strtotime("+1 year"));
  }

  static public function GetCurrentAcademicHalf() {
    return date("m") < 9 ? 2 : 1;
  }

  static public function GetCurrentAcademicDate() {
    return self::GetCurrentAcademicHalf() . "/" . self::GetCurrentAcademicYear();
  }

  static protected function GetStudygroupsArray() {
    $db = new Db();
    $l = $db->getData("SELECT * FROM studygroups");
    return $l;
  }

  static protected function GetTeachersArray() {
    $db = new Db();
    $l = $db->getData("SELECT * FROM teachers");
    return $l;
  }

  static protected function GetCabinetsArray() {
    $db = new Db();
    $l = $db->getData("SELECT * FROM cabinets");
    return $l;
  }

  static protected function GetStudygroupsNamesArray($studygroups = null) {
    if ($studygroups === null) {
      $studygroups = self::GetStudygroupsArray();
    }
    $names = [];

    foreach($studygroups as $s) {
      array_push($names, $s["name"]);
    }
    return $names;
  }

  static protected function GetTeachersNamesArray($teachers = null) {
    if ($teachers === null) {
      $teachers = self::GetTeachersArray();
    }
    $names = [];

    foreach($teachers as $s) {
      array_push($names, $s["name"]);
    }
    return $names;
  }

  static protected function GetCabinetsNamesArray($cabinets = null) {
    if ($cabinets === null) {
      $cabinets = self::GetCabinetsArray();
    }
    $names = [];

    foreach($cabinets as $s) {
      array_push($names, ["block" => $s["block"], "number" => $s["number"]]);
    }
    return $names;
  }

  static public function GetUID($json) {
    return md5($json);
  }

  static public function VerifyScheduleList($data) {
    if (is_array($data)) {
      foreach ($data as $d) {
        if (
          !empty($d->name) &&
          !empty($d->teacher) &&
          !empty($d->cabinet)
        ) {
          return true;
        }
      }
    }
    return false;
  }

  static public function VerifyStudygroup($studygroup, $studygroups = null) {
    if ($studygroups === null) {
      $studgroups = self::GetStudygroupsNamesArray();
    }
    return in_array($studygroup, $studgroups);
  }

  static public function VerifyTeacher($teacher, $teachers = null) {
    if ($teachers === null) {
      $teachers = self::GetTeachersNamesArray();
    }
    return in_array($teacher, $teachers);
  }

  static public function VerifyCabinet($block, $number, $cabinets = null) {
    if ($cabinets === null) {
      $cabinets = self::GetCabinetsNamesArray();
    }
    return in_array(["block" => $block, "number" => $number], $cabinets);
  }

  static public function BuildScheduleObject($date, $studygroup, $list) {
    return [
      "date" => $date,
      "group" => $studygroup,
      "schedule" => $list
    ];
  }

}