<?php

use Slim\Psr7\Request;

require_once("db.php");

class AuthHelper {
  static function Authorize(Request $request) {
    $token = $request->getHeader("Authorization") ?? null;
    if (!$token) {
      return false;
    } else {
      $token = self::GetTokenFromHeader($token[0]); 

      if (!$token) {
        return false;
      }

      $date = date("Y-m-d H:i:s", strtotime("-2 days"));
      $db = new Db();
      $search = $db->getData("SELECT ID FROM tokens WHERE token='{$token}' AND time > '{$date}'");

      if (count($search) < 1) {
        return false;
      } else {
        return true;
      }      
    }
  }

  static function GetNewToken($login) {
    return md5($login . date("Y-m-d H:i:s"));
  }

  static function GetTokenFromHeader($header) {
    return count(explode("Bearer ", $header)) > 1 ?
      explode("Bearer ", $header)[1] :
      false;
  }

}