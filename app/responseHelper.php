<?php

require_once("db.php");

use Psr\Http\Message\ResponseInterface as Response;

class ResponseHelper {
  static function BadTokenResponse(Response $response) {
    $response->getBody()->write(json_encode([
      "status" => StatusEnum::ERR,
      "errorText" => "Bad authorization token",
      "errorCode" => "BAD_TOKEN"
    ]));
    return $response;
  }

  static function BadInputResponse(Response $response) {
    $response->getBody()->write(json_encode([
      "error" => 500,
      "errorText" => "Bad Input",
      "errorCode" => "BAD_INPUT"
    ]));
    return $response;
  }
}