<?php
require 'config.php';

class Db
{
  function __construct()
  {
    $this->config = new Config();
    $this->connect = null;
    $this->getConnect();
    unset($this->config);
    $this->connect->set_charset("utf8");
  }

  function getConnect()
  {
    $this->connect = new mysqli(
      $this->config->db_host,
      $this->config->db_user,
      $this->config->db_pass,
      $this->config->db_name
    );

    if ($this->connect->connect_error) {
      $err = $this->connect->connect_error;
      $this->connect = null;
      die($err);
      throw new RuntimeException("Error while DB connection.", 500);
    }
  }

  function doSql($sql)
  {
    if ($this->connect == null)
      die("No connection found");


    return $this->connect->query($sql);
  }

  function getData($sql)
  {
    $data = $this->connect->query($sql);
    $r = [];

    if (!$data)
      return false;

    while ($row = $data->fetch_assoc())
      array_push($r, $row);

    return $r;
  }
}
